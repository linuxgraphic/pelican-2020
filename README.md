# linuxgraphic.org

static Website for linuxgraphic.org

- using pelican
- with the news and tutorials as markdown files
- showing a an rss feed with the latest forums posts

## How to

- Bigger images should not be added to the repository and for sure not added to the pelican output synced to the site
  - wordpress 
- prefix the file name of your news articles and tutorials with the year
- always set a Slug:
  - it should be as short as possible
  - it should not clash with past and future articles with a similar topic
- always create a cover image
  - it's a 400x100 png image.
  - place it in the `content/images/` directory.
  - name it `markdown-file-name-cover.png`

## Pelican

- create the static files: `$ pelican content`
- serve the static files and autoreload: `$ pelican -r --listen`

## Publishing

The source files for the Pelican site are collected in a Gitlab directory:  
<https://gitlab.com/linuxgraphic/pelican-2020/>

You can fork this repository, make your changes and create a merge request if you want to:

- modify the theme and the look and feel of the wesite
- add a news article or a tutorial

The static files that published on the website are also synced through a Gitlab repository:  
<https://gitlab.com/linuxgraphic/pelican-2020-static-html/>

The static files are published:

- by first cloning the repository, then generating the content over the downloaded file,
- then committing and pushing the changes,
- and finally automatically synced with the site through a Gitlab webhook.

Short:

```sh
git clone git@gitlab.com:linuxgraphic/pelican-2020-static-html.git output
pelican content
cd output
git commit -a -m "commit message"
git push
```

## Latest forum posts

- The feed is read from the same origin (if the feed comes from a different source, you will need the source to have the right CORS headers or you can setup a php/py/js proxy on your server)
- The forums' feed is: <https://linuxgraphic.org/forums/feed.php>
- [FAQ: phpBB ATOM feeds](https://www.phpbb.com/support/docs/en/3.3/kb/article/faq-phpbb-atom-feeds/)

In order to test the javascript locally

- Create a `content/forum-rss/index.md/` page that is saved as `forum-rss.xml` and uses a `forum-rss.html` template that contains a static copy of the RSS feed's XML.
- `localhost:8000/forums-rss.xml` should then correctly serve a copy of the feed.

It's a custom javascript script with no dependencies:

- this has a few good hints: <https://github.com/hongkiat/js-rss-reader/blob/master/rss.js>
- this too: <https://www.hongkiat.com/blog/rss-reader-in-javascript/> ([with code on Github](https://github.com/hongkiat/js-rss-reader/blob/master/rss.js)).
- <https://github.com/mortada/pelican_javascript> might help inserting the javascript in the main page.

## Forms

- [How to build a Pelican contact form](https://www.unixdude.net/posts/2017/Nov/29/pelican-contact-form/)

## Videos

- create a plugin similar to <https://github.com/kura/pelican_youtube> for markdown and peertube?

## Tuxfamily

The idea is to get tuxfamily to update the html files from a git repository with the static files.

- by downloading a tar.gz
- or by synchrinizing through git

- Before you can start Python on Tuxfamily you need to get into the group of your project:
  - `newgrp project-name`
  - (or, at least, you need it before running `-m venv`)
  - <https://faq.tuxfamily.org/WebArea/Compat/Python>

## Exporting from Wordpress

- <https://github.com/some-programs/exitwp>
- `virtualenv -p python2 venv`
- use the `link` and `slug`  fields to create the redirection

## Other possible static site generators

Python:

- [Cactus](https://github.com/eudicots/Cactus):
  - with Django template system
- [MakeSite](https://github.com/sunainapai/makesite)
  - Very small
  - completely hackable
- [Hyde](http://hyde.github.io/)
- [Nikola](https://getnikola.com/)
- [Mynt](https://mynt.uhnomoli.com/)
- [Lektor](https://github.com/lektor/lektor)

## Inspiration

- https://gitlab.gnome.org/Infrastructure/gimp-web/
- https://www.gimp.org/about/meta/using-pelican/
