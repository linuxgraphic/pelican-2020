#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# SITE_TARGET = 'localhost'
SITE_TARGET = 'linuxgraphics_test'
# SITE_TARGET = 'linuxgraphics'
# SITE_TARGET = 'xox_test'

AUTHOR = 'a.l.e'
SITENAME = 'LinuxGraphic'
if SITE_TARGET == 'localhost':
    SITEURL = ''
elif SITE_TARGET == 'xox_test':
    SITEURL = 'http://ww.xox.ch/test'
elif SITE_TARGET == 'linuxgraphics_test':
    SITEURL = 'https://linuxgraphic.org/test'
elif SITE_TARGET == 'linuxgraphics':
    SITEURL = 'https://linuxgraphic.org'

SITESUBTITLE = 'Discussions, tutoriels, et actualtés autour du graphisme avec logiciel libre'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
# PAGE_SAVE_AS = '{slug}/{filename}'

CATEGORY_URL = '{slug}/'
CATEGORY_SAVE_AS = '{slug}/index.html'

USE_FOLDER_AS_CATEGORY = True
ARTICLE_URL = '{category}/{slug}/'
ARTICLE_SAVE_AS = '{category}/{slug}/index.html'

# PAGE_PATHS = ['accueil', 'forums', 'participer']
PAGE_PATHS = ['accueil', 'participer', 'participer']
ARTICLE_PATHS = ['actualites', 'tutoriels']
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

STATIC_PATHS = ['images']

if SITE_TARGET in ('localhost', 'xox_test'):
    STATIC_PATHS += ['xml']

SUMMARY_MAX_LENGTH = 40

THEME = 'themes/linuxgraphic'

MENUITEMS = (
    ('Accueil', ''),
    ('Actualités', 'actualites/'),
    ('Tutoriels', 'tutoriels/'),
    ('Forums', 'forums/'),
    ('Participer', 'participer/')
)

IGNORE_FILES = ['\\.sw.$', 'README.md'] # ignore vim swap files

if SITE_TARGET == 'localhost':
    # FORUM_FEED_URL = SITEURL + '/xml/forum-rss.xml'
    FORUM_FEED_URL = SITEURL + '/xml/topic-rss.xml'
elif SITE_TARGET == 'xox_test':
    FORUM_FEED_URL = SITEURL + '/xml/forum-rss.xml'
elif SITE_TARGET == 'linuxgraphics_test':
    FORUM_FEED_URL = 'https://linuxgraphic.org/forums/feed.php'
elif SITE_TARGET == 'linuxgraphics':
    FORUM_FEED_URL = SITEURL + '/forums/feed.php'

def link_from_outputfile(value):
    """ jinja filter that allows the detection of the current nav item """
    parts = str(value).split('/')
    if len(parts) == 1:
        return ''
    return parts[0] + '/'

JINJA_FILTERS = {
    'link_from_outputfile': link_from_outputfile,
    }
