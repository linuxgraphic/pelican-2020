Title: Forme personnalisée avec une texture de bois vectorielle
Date: 2020-08-15 13:20
Category: tutoriels
Author: Imppao
cover: images/forme-personalisee-texture-bois-cover.png
comments: t=9284

Nous allons découvrir comment créer une forme personnalisée avec une texture de bois vectorielle.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/1b0d36a4-5445-45e2-a031-1602eb341cee" frameborder="0" allowfullscreen></iframe>

Basé sur [Inkscape for Beginners: Custom Shape with a Vector Wood Texture](https://www.youtube.com/watch?v=ztL3ztmYLVk) par [Logos by Nick](https://www.youtube.com/channel/UCEQXp_fcqwPcqrzNtWJ1w9w).
