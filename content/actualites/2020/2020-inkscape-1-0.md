title: Inkscape 1.0
date: 2020-08-15 13:20
slug: inkscape-1-0
author: a.l.e
category: actualites
tags: inkscape
cover: images/2020-inkscape-1-0-cover.png
accueil: Inkscape 1.0 a été publié le 1er mai 2020.

Inkscape 1.0 a été publié le 1er mai 2020.

Il ne s'agit pas seulement d'un gros pas dans la numérotation du logiciel, de nombreuses fonctionalités ont été ajoutées our améliorées:


- Theming support and more new customization options
- Better HiDPI (high resolution) screen support
- Native support for macOS with a signed and notarized .dmg file
- Coordinate origin in top left corner by default
- Canvas rotation and mirroring
- On-Canvas alignment of objects
- Split view and X-Ray modes
- PowerPencil for drawing editable, variable width strokes with a pressure sensitive graphics tablet
- New PNG export options
- Integrated centerline tracing for vectorization of line drawings
- Searchable Symbols dialog
- New Live Path Effect (LPE) selection dialog
- New Corners (Fillet/chamfer) LPE, (lossless) Boolean Operation LPE (experimental), Offset LPE and Measure Segments LPE (and more!)
- Path operations, deselection of a large number of paths as well as grouping/ungrouping are much faster now
- Much improved text line-height settings
- Variable fonts support (only if compiled with pango library version >= 1.41.1)
- Browser-compatible flowed text
- Extensions programming interface updated, with many new options
- Python 3 support for extensions

