title: Inkscape 0.92 est arrivé
date: 2017-01-12 14:00
link: https://www.linuxgraphic.org/wp/inkscape-0-92-arrive/
slug: inkscape-0-92
author: Elisa
category: actualites
tags: inkscape
cover: images/2017-inkscape-0-9-2-cover.png
accueil: Inkscape 0.92 a été publié le 1er janvier 2020.

Merci à toute l'équipe d'Inkscape sur le travail de cette nouvelle version d'Inkscape sobrement intitulé 0.92.

[![](https://download.tuxfamily.org/linuxgraphic/wp/2017/01/inkscape092.png)](https://download.tuxfamily.org/linuxgraphic/wp/2017/01/inkscape092.png)

Téléchargez ou mettez à jour [votre logiciel](https://inkscape.org/fr/) pour utiliser les nouveautés. Jabier a préparé [cette vidéo](https://inkscape.org/en/~jabiertxof/%E2%98%85inkscape-092-showcase) résumant les principales nouveautés en 1min 20s.


Les nouveautés évidentes:
 	
1. Nouvel outil : filet de dégradé (les vidéos de [heathenyxt](https://www.youtube.com/watch?v=c73D8-I21oU) ou de [Edson Santos](https://www.youtube.com/watch?v=mW5_m3Y_0y8) en parlent).
2. Passage du canevas de 90 ppp à 96 ppp (normes CSS obligent), du coup, Inkscape convertira votre canevas tout seul à l'ouverture de tous vos anciens fichiers, n'ayez pas peur, laissez-le faire.
3. Apparition d'un nouveau dialogue destiné aux Symboles et aux Sélections et améliorations des dialogues Objets et Calques.
4. Les guides sont maintenant verrouillables.
5. Améliorations des outils Aérographe, Mesurer et Stylo.
6. Nouvelles courbes B-Splines.
7. De nouvelles extensions et effet de chemin améliorés.

Pour connaître tous les changements, lisez la [Release note traduite en français](http://wiki.inkscape.org/wiki/index.php/Release_notes/0.92/fr#Joindre_un_chemin) (déjà!).

Et surtout bon Inkscape et bonne année 2017 !
