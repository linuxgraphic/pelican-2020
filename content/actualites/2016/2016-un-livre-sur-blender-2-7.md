title: Un livre sur Blender 2.7
date: 2016-02-13 15:38
link: https://www.linuxgraphic.org/wp/un-livre-sur-blender-2-7/
slug: un-livre-sur-blender-2-7
author: dimitri
category: actualites
tags: blender, livre
cover: images/2016-un-livre-sur-blender-2-7-cover.png

Ronan Ducluzeau a publié un [livre sur la série 2.7 de Blender](http://www.editions-eni.fr/livres/blender-2-7/.a54c747694211bf0bc385634d6b9c7d2.html), disponible aux [éditions ENI](http://www.editions-eni.fr/).

[![Blender 2.7 - Ronan Ducluzeau](https://download.tuxfamily.org/linuxgraphic/wp//2016/02/blender-2.7-ronan-ducluzeau-248x300.jpg)](http://www.editions-eni.fr/livres/blender-2-7/.a54c747694211bf0bc385634d6b9c7d2.html)

Quelques années ont passé depuis la publication de son premier livre sur la série 2.6. Il était temps de renouveler les références aux manipulations de bases dans Blender afin de travailler plus efficacement.

Beaucoup d'étapes rébarbatives ont été simplifiées dans les versions 2.7. Beaucoup de nouveautés ont accru les capacités de Blender. Ce livre les décrit tout en abordant des fonctions poussées, laissées de côté lors de la première édition pour manque de place ou de maturité. La [table des matières](http://www.editions-eni.fr/livres/blender-2-7/.a54c747694211bf0bc385634d6b9c7d2.html) est consultable sur le site de l'éditeur ainsi que le début du chapitre 6 « [Créer un maillage (Mesh)](http://www.editions-eni.fr/_Download/09a8b5da-ed27-47e5-946a-37dbac19d6ee/Blender-2-7_(Extrait-du-livre).pdf) ».

Le livre est accessible aux débutants et utile à ceux qui n'ont pas eu le temps de suivre les nouvelles de la communauté Blender. Même si il est difficile de faire une sélection parmi tous les outils qu'offrent Blender ; le livre devrait compléter assez harmonieusement les autres sources d'informations disponibles pour donner une image fidèle du stade du logiciel.
