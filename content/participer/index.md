title: Participer
date: 2020-08-15 13:20
slug: participer
author: a.l.e

## Le forum

La façon la plus simple pour participer à la communauté de LinuxGraphic.org est de s'inscrire aux [forums](/forums).

Tu peux poser des questions, répondre aux questions des autres ou – dans [La vie du site](/forums/viewforum.php?f=2) – discuter avec les autres membres de la communauté.

## Les tutoriels

Si tu veux proposer un tutoriel, tu as deux possibilités:

- faire un clone du [dépôt git](), ajouter le tutoriel dans `content/tutoriels/2020/` et faire une merge request.
- ajouter le tutoriel dans les forums (en entier ou par un lien) et nous demander de la publier.

Dans les deux cas nous aurons besoin:

- d'une brève description pour la page d'accueil (le champ `accueil` dans l'en-tête du fichier markdown)
- d'une image PNG 400 x 100 (le champ `cover` dans l'en-tête du fichier markdown)

## L'actualité

Si tu veux proposer un article dans les actualités, tu as deux possibilités:

- faire un clone du [dépôt git](), ajouter l'article dans `content/actualités/2020/` et faire une merge request.
- ajouter la nouvelle dans les forums (en entier ou par un lien) et nous demander de la publier.

Dans les deux cas nous aurons besoin:

- d'une brève description pour la page d'accueil (le champ `accueil` dans l'en-tête du fichier markdown)
- d'une image PNG 400 x 100 (le champ `cover` dans l'en-tête du fichier markdown)

## How-to

### L'en-tête des fichier Markdown

Chaque article doit commencer avec les champs suivants:

```yaml
title: Le titre
date: 2020-08-15 13:20
slug: ton-article
author: Ton Nom
category: actualites|tutoriels
cover: images/2020-ton-article-cover.png
```

Les champs facultatifs sont

- `accueil: Une brève description pour la liste sur la page d'accueil`  
  Si aucune description n'est définie, les premiers mots de l'article seront affichés (sans balises html).
- `tags: inkscape|gimp|scribus|...`
- `comments: t=9284`  
  si tu souhaites activer les commentaires, tu peux créer un fil de discussion dans le forum et ajouter son numéro pour la variable `t`.

### Lien vers une vidéo externe

Tu peux utiliser le lien de partage de la plateforme (un `iframe`) ou ajouter un lien vers la vidéo par dessus une image d'aperçu:

```html
<div class="video">
<a href="https://peertube.mastodon.host/videos/watch/1b0d36a4-5445-45e2-a031-1602eb341cee"><img src="https://peertube.mastodon.host/static/previews/1b0d36a4-5445-45e2-a031-1602eb341cee.jpg"></a>
<a href="https://peertube.mastodon.host/videos/watch/1b0d36a4-5445-45e2-a031-1602eb341cee" class="play"></a>
</div>
```

